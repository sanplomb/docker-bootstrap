Docker Bootstrap
================

This is my personal docker configuration to start a new project.

Docker configuration available :

* Static html project
* PHP Project without database or use SQLite
* PHP Project connected to MySQL Server
* PHP Project connected to PostgreSQL Server

Each dockerfile can be used with Apache or Nginx for Web Server. By default Apache and Nginx are setting up on different ports, so both can be turn on in the same time.

Feel free to use it and personalize it for your needs.

How to use
----------

Clone this repo
```Shell
git clone https://sanplomb@bitbucket.org/sanplomb/docker-bootstrap.git
```

Suppress the repo
```Shell
rm -rf .git
```

Initiate your repo
```Shell
git init
git remote add origin your_remote_repo
```

Keep only files needed for your project
